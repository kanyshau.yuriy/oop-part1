package com.ehu.esde.oop;

public class Counter {
    private int counterState;

    private int defaultCounterStep;
    public Counter() {
        this.setDefaultCounterStep(1);
        this.setCounterState(0);
    }

    public Counter(int defaultCounterStep, int counterState) {
        this.setDefaultCounterStep(defaultCounterStep);
        this.setCounterState(counterState);
    }

    public int getDefaultCounterStep() {
        return defaultCounterStep;
    }

    public void setDefaultCounterStep(int defaultCounterStep) {
        this.defaultCounterStep = defaultCounterStep;
    }

    public void setCounterState(int counterState) {
        this.counterState = counterState;
    }

    public int getCounterState() {
        return counterState;
    }

    public void increaseCounter(int increase) {
        this.counterState += increase;
    }
    public void decreaseCounter(int decrease) {
        this.counterState -= decrease;
    }

    public void increaseCounter() {
        this.counterState += this.getDefaultCounterStep();
    }
    public void decreaseCounter() {
        this.counterState -= this.getDefaultCounterStep();
    }
}
