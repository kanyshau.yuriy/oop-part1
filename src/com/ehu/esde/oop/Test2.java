package com.ehu.esde.oop;

public class Test2 {
    private int firstNumber;
    private int secondNumber;

    public Test2() {
        this.setFirstNumber(1);
        this.setSecondNumber(1);
    }
    public Test2(int firstNumber, int secondNumber) {
        this.setFirstNumber(firstNumber);
        this.setSecondNumber(secondNumber);
    }

    public void setSecondNumber(int secondNumber) {
        this.secondNumber = secondNumber;
    }

    public int getSecondNumber() {
        return secondNumber;
    }

    public void setFirstNumber(int firstNumber) {
        this.firstNumber = firstNumber;
    }

    public int getFirstNumber() {
        return firstNumber;
    }
}
